\section{Replacement Policy}
\label{chapter:replacement_policy}

The line replacement policy in a k-way set-associative cache is implemented by
the module shown in Figure~\ref{fig:replacement_policy}. Different available
replacement policies can be selected using the "REP\_POLICY" synthesis
parameter. The module has three main components: the Policy Info Memory (PIM),
the Policy Info Updater (PIU) datapath, and the Way Select Decoder
(WSD). Table~\ref{table:replacement_policy} explains the ports of the module.

The PIM stores information pertaining to the implemented policy. Note that
replacement policies are dynamic and use data from the past, so memory is
needed. The PIM has as many positions as the number of cache sets, addressed by
the {\it index} part of the main memory address. The width of the PIM depends on
the chosen policy. The PIM is implemented using a register-file so that during
a system reset or cache invalidation, it can be set to default initial values.

When a cache hit is detected, the information stored in the PIM is updated based
on the information previously stored for the respective set and the newly
selected way. This function is performed by the PIU. When a cache miss is
detected the information for the respective cache set is read from the PIM and
analyzed by the WSD in order to choose the way where the cache line will be
replaced.

\begin{figure}[!h!tp]
  \centering
  \includegraphics[width=0.9\linewidth]{Figures/replacement_policy}
  \caption{Replacement Policy module diagram.}
  \label{fig:replacement_policy}
\end{figure}

\begin{table}[h]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Replacement policy ports.}
  \label{table:replacement_policy}
  \centering
  \begin{tabular}{||l||r|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\
    \hline
    \textbf{write\_en}  & 1  & input  &  Enable signal to update the memory. Cache-Memory's data\_ready signal.\\\hline
    \textbf{addr}  & LINE\_OFF\_W  & input  &  Address of the respective cache line. \\\hline
    \textbf{way\_hit}  & N\_WAYS  & input  &  The signal that indicates in which way occurred a hit, one-hot format. Used in the encoder.  \\\hline
    \textbf{reset}  & 1  & input  &  Resets the replacement policy memory's data. Used during a reset of the system or a cache invalidate.  \\\hline
    \textbf{way\_select}  & N\_WAYS  & output  &  The selected way decoded from the current replacement's key. Used to select a way during a line-replacement. In one-hot format. \\\hline
    \textbf{way\_select\_bin}  & $log_2$(N\_WAYS)  & output  &  The same as way\_select but in the binary format. Uses a one-hot-to-binary encoder.\\\hline
  \end{tabular}
\end{table}

The currently implemented policies are the Least-Recently-Used (\ac{LRU}) and
the Pseudo-Least-Recently-Used (tree and \ac{MRU}-based). These are explained in
the next subsections.


\clearpage
\subsection{Least-Recently-Used}
\label{chapter:lru}

The Least-Recently-Used policy (\ac{LRU}) needs to store, for each set, a word
that has N\_WAYS fields of log2(N\_WAYS) bits each. Each field, named "mru[i]",
represents how recently the way has been used by storing a number between 0
(least recently used) and N\_WAYS-1 (most recently used), thus requiring
$log_2$(N\_WAYS) bits. In total it requires $N\_WAYS log_2(N\_WAYS)$ bits per
cache set.

\begin{figure}[h]
  \centering
  \includegraphics[width=.55\linewidth]{Figures/replacement_LRU_encoder}
  \caption{LRU Encoder datapath flowchart.}
  \label{fig:lru_encoder}
\end{figure}

The way each mru[i] is updated is represented in
Figure~\ref{fig:lru_encoder}. Summarizing, when a way is accessed either by
being hit or replaced, it becomes the most recently used and is assigned.  The
other ways with higher mru values than the accessed way get decremented.  The
ones with lower mru values are unchanged. The selected way for replacement is the
one with the lowest "mru" index. This can be achieved by NORing each index, as
implemented in Equation~\ref{eq:lru_decoder}.

\begin{equation}
  \text{way\_select [i] = !OR(mru[i])}\,.
  \label{eq:lru_decoder}
\end{equation}

\subsection{Pseudo-Least-Recently-Used: MRU-based}
\label{chapter:plru_mru}

The \ac{PLRUm} is simpler than the \ac{LRU} replacement and needs to store, for
each set, a word that has N\_WAYS bits only. Each bit mru[i] represents how
recently the way has been used, storing a 0 (least recently used) or 1 (most
recently used), thus requiring $log_2$(N\_WAYS) bits.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.4\linewidth]{Figures/replacement_PLRU_mru_encoder}
  \caption{PLRUm Updater datapath flowchart.}
  \label{fig:plru_mru_encoder}
\end{figure}

The way each mru[i] is updated is represented in
Figure~\ref{fig:plru_mru_encoder}. Summarizing, when a way is accessed either by
being hit or replaced, the respective bit is assigned 1 meaning it has been
recently used. When all ways have been recently used, the most recently assigned
remains asserted and the others are reset. This is done by simply ORing the
way\_hit signal and the stored bits, or storing the way\_hit signal if all have
been recently used. To select a way for replacement, the not recently used way
(mru[i]=0) with the lowest index is selected.  This can be implemented by the
following logic equation, Equation~\ref{eq:plrum_decoder}.

\begin{equation}
  \text{way\_select [i] = !mru[i] AND (AND(mru[i-1:0])}
  \label{eq:plrum_decoder}
\end{equation}



\subsection{Pseudo-Least-Recently-Used: binary tree-based}
\label{chapter:plru_tree}

The \ac{PLRUt} needs to store, for each set, a binary tree with $log_2(N\_WAYS)$
levels and $N\_WAYS$ leaves, each representing a cache way. Each level divides
the space to find the way in two, creating a path from the root node to the
chosen way, when traversed by the WSD. Each node is represented by a bit b[i]
where 0 selects the lower half and 1 selects the upper half of the space. For a
8-way example, the binary tree is represented in Figure~\ref{fig:plru_tree}.

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{Figures/replacement_PLRU_tree}
  \caption{PLRU binary tree.}
  \label{fig:plru_tree}
\end{figure}


In order to update each node b[i], the first step is to get the slice
way\_hit[i] from the vector way\_hit, relevant for computing
b[i]. Figure~\ref{fig:compute_hit_slice} shows how to compute way\_hit[i] for
the first 3 notes, b[2:0]. After computing slice way\_hit[i], the algorithm
shown in Figure~\ref{fig:plru_tree_encoder} is followed. The process is
straightforward. If the slice is not hit (all its bits are 0), then b[i] remains
unchanged. Otherwise, b[i] is set to 0 if the hit happens in the upper part of
the slice and to 1 if the hit happens in the lower part.

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=0.8\linewidth]{Figures/replacement_PLRU_tree_encoder}
  \caption{Computing way\_hit slices.}
  \label{fig:compute_hit_slice}
\end{figure}

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=0.50\linewidth]{Figures/replacement_PLRUt_encoder}
  \caption{PLRU way updater.}
  \label{fig:plru_tree_encoder}
\end{figure}

To select the way for doing the replacement, the binary tree needs to be
decoded. This can be done by iterating from the tree levels, from root to
leaves, using the b[i] values to point to the next node until the leaf is
reached. As explained before the leaf index is the chosen way.

