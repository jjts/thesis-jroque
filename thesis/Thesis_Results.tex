\chapter{Results}
\label{chapter:results}

This chapter presents results on IOb-Cache's performance and FPGA
implementation. A comparison between IOb-Cache and other open-source caches is
also presented.


\section{Performance}
\label{chapter}

The Dhrystone~\cite{bib:dhrystone_paper} benchmark is a useful tool for
measuring the performance of processors, using the Dhrystones/s score.  The
frequency-independent Dhrystone score is called Dhrystone \ac{MIPS}/MHz or
simply DMIPS. Here the Dhrystone benchmark is used to indirectly evaluate the
cache as the performance of the system translates the performance of the cache
if the processor remains the same. To do that, the \ac{CPI} measurement is taken
while running the benchmark, as it provides a more direct indication of the
cache performance compared to the DMIPS figure.

To efficiently test the cache, a pipelined processor is required, with a
performance close to 1 CPI when using an ideal RAM memory. This way it is
possible to analyze the average delay of the cache during memory accesses,
Equation~\ref{eq:aat}.

The Dhrystone benchmark has one shortcoming for testing the various policies, it
is a small program that can be fitted entirely inside an instruction cache of
common size. This shortcoming becomes an advantage for testing the pipeline
operation, since it after full it behaves like a RAM, in a system connected to a
larger SDRAM. Being a RAM, the correct pipeline operation happens with
consecutive loads and stores which should take 1 cycle per instruction. A
correct cache design allows for 1-cycle loads and stores whereas a poorer design
will need 2 cycles for load/store instructions.

The tests are run in IOb-SoC~\cite{bib:iob_soc} (Section~\ref{chapter:iob_soc}),
using the SSRV~\cite{bib:ssrv,bib:iob_ssrv} multi-issue superscalar RISC-V
processor. Despite being multi-issue, the processor was limited to 1 instruction
per clock cycle in the tests, which is a simple setup but allows testing the
cache. Connected to the IOb-SoC's internal memory (RAM only and no cache), it
achieved CPI=1.02, running for 40445 clock cycles.  The cache is implemented
following the structure represented in
Section~\ref{chapter:iobcache_multilevel}: an L1-Instruction and L1-Data caches
connected to an L2-Unified cache. In the subsections below simulation and FPGA
results are presented.


\subsection{Simulation}
\label{chapter:simulation}

The simulation results are displayed in Table~\ref{table:performance_sim_ssrv},
with the cache is connected to an AXI4 RAM.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Simulation Dhrystone SSRV (IOb-SoC) 32-bit. 100 runs using gcc -O1
    optimization. Parameters: number of ways (repl. policy), lines, words per
    line.}
  \label{table:performance_sim_ssrv}
  \centering
  \begin{tabular}{||r|r|r||r|l|}
    \hline \textbf{L1-Instr} & \textbf{L1-Data} & \textbf{L2-Unified} &
    \textbf{clock cycles} & \textbf{CPI} \\\hline
    \multicolumn{5}{|c|}{\textbf{48 B - Minimum size}}\\\hline 1, 2, 2 & 1, 2, 2
    & 1, 2, 2 & 319580 & 8.066\\\hline \multicolumn{5}{|c|}{\textbf{2
        KB}}\\\hline 2 (LRU), 8, 8 & 2 (LRU), 8, 8 & 4 (PLRUm), 8, 8 & 162147 &
    4.092\\\hline 2 (PLRUm), 8, 8 & 2 (PLRUm), 8, 8 & 4 (PLRUm), 8, 8 & 162147 &
    4.092\\\hline 2 (PLRUt), 8, 8 & 2 (PLRUt), 8, 8 & 4 (PLRUm), 8, 8 & 162147 &
    4.092\\\hline 1, 16, 8 & 1, 16, 8 & 4 (PLRUm), 8, 8 & 174620 & 4.407\\\hline
    1, 16, 8 & 1, 16, 8 & 1, 16, 16 & 204016 & 5.149\\\hline
    \multicolumn{5}{|c|}{\textbf{4 KB}}\\\hline 4 (LRU), 8, 8 & 4 (LRU), 8, 8 &
    8 (LRU), 8, 8 & 95331 & 2.406\\\hline 4 (LRU), 8, 8 & 4 (LRU), 8, 8 & 8
    (PLRUm), 8, 8 & 87031 & 2.196\\\hline 4 (LRU), 8, 8 & 4 (LRU), 8, 8 & 8
    (PLRUt), 8, 8 & 90417 & 2.282\\\hline 4 (PLRUm), 8, 8 & 4 (LRU), 8, 8 & 8
    (PLRUm), 8, 8 & 79310 & 2.001\\\hline 4 (PLRUt), 8, 8 & 4 (LRU), 8, 8 & 8
    (PLRUm), 8, 8 & 84854 & 2.141\\\hline 4 (PLRUm), 8, 8 & 4 (PLRUm), 8, 8 & 8
    (PLRUm), 8, 8 & 79310 & 2.001\\\hline 4 (PLRUm), 8, 8 & 4 (PLRUt), 8, 8 & 8
    (PLRUm), 8, 8 & 79310 & 2.001\\\hline 1, 64, 4 & 1, 64, 4 & 1, 64, 8 &
    107668 & 2.717\\\hline \multicolumn{5}{|c|}{\textbf{8 KB}}\\\hline 2, 16, 16
    & 2, 16, 16 & 4 (LRU), 16, 16 & 50758 & 1.281\\\hline 2, 16, 16 & 2, 16, 16
    & 4 (PLRUm), 16, 16 & 50751 & 1.281\\\hline 2, 16, 16 & 2, 16, 16 & 4
    (PLRUt), 16, 16 & 50758 & 1.281\\\hline 1, 32, 16 & 1, 32, 16 & 1, 64, 16 &
    77306 & 1.951\\\hline
    1, 64, 8 & 1, 64, 8 & 1,128, 8 & 71543 & 1.805\\\hline
    \multicolumn{5}{|c|}{\textbf{16 KB}}\\\hline
    4, 16, 16 & 4, 16, 16 & 8, 16, 16 & 41837 & 1.055\\\hline
    4, 32, 8 & 4, 32, 8 & 8, 32, 8 & 41762 & 1.055\\\hline
    2, 64, 8 & 2, 64, 8 & 4, 64, 8 & 41886 & 1.057\\\hline
    1, 64,16 & 1, 64, 16 & 1, 128, 16 & 56848 & 1.434\\\hline
    1, 128, 8 & 1, 128, 8 & 1, 128, 16 & 54986 & 1.387\\\hline
    \multicolumn{5}{|c|}{\textbf{32 KB}}\\\hline
    8, 16, 16 & 8, 16, 16 & 16, 16, 16 & 41837 & 1.055\\\hline
    2, 128, 8 & 2, 128, 8 & 4, 128, 8 & 41762 & 1.054\\\hline
    1, 256, 8 & 1, 256, 8 & 1, 256, 16 & 41811 & 1.055\\\hline
    2, 64, 16 & 2, 64, 16 & 4, 64, 16 & 41837 & 1.055\\\hline
  \end{tabular}
\end{table}

The minimum possible size for 2-level configuration is 48 Bytes, 16 Bytes for
each of the 3 caches. This is the worst possible scenario performance-wise.  If
the L1s do not have the requested word, neither does the L2. The large delay in
between instructions is caused by the high miss rate, causing accesses to the
main memory, as well as traffic congestion between the L1s and L2 accesses.

Using 2 KB caches, one can see there is no performance difference between the
replacement policies in a 2-way set-associative cache. The way selected is the
one that was not the most recently used in all cases.  It also shows the
difference in performance between the set-associative and directly mapped
cache. Using a set-associative in the L2-Unified cache represents the largest
improvement in performance (up to 0.315 CPI). If the three caches only use
direct mapping, the performance drops by 25.8\%.

Using 4 KB caches highlights the differences in performance of the different
replacement policies. The \ac{PLRUm} policy displays the highest performance in
all three caches, while the \ac{LRU} policy gives the worst performance. The
reduced size of the L1-Instruction (1 KB), and the firmware's instruction loops
constitutes an environment where replacing the least recently used is not
effective, due to low time locality. The PLRU policies lack memory compared to
the LRU and are worse at identifying the most recently used line. However, this
ends up not being a handicap as there is no time locality to exploit. The
L2-Unified is more likely to see a performance improvement with PLRU
policies~\citep{bib:cache_study,bib:cache_perf}. This results from the fact L2
is accessing different memory blocks (instructions and data) with inherently low
time locality.

Using 16 KB and 32 KB caches, the size is large enough to fit the program. There
is no change in performance between the different replacement policies. Despite
the program being 25 KB in size and the L1-Instruction caches 4 KB and 8 KB,
respectively, the program is not required to fit entirely in these memories. As
the program is executed, the only misses that occur are the initial compulsory
misses, followed by capacity misses that replace the previous non-looping
instructions. As the caches are big enough to store all recently looping code,
conflict misses becoming inexistent.

In all the previous examples, the choice of the size for the L1 data cache has
little significance since the Dhrystone benchmark is an instruction-intensive
program.

\clearpage
\subsection{FPGA}
\label{chapter:fpga}


The FPGA system is implemented in the ultrascale FPGA, as described in
Section~\ref{chapter:iob_soc}.  It uses a 50 MHz clock, 1/4 of the Memory
Interface's frequency, so it requires the implementation of the synchronous
AXI-Interconnect with a 1:4 clock ratio.  The results are presented in
Table~\ref{table:performance_fpga_ssrv}.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{FPGA emulation of Dhrystone SSRV (IOb-SoC) 32-bit at 50 MHz. 100 runs
    using gcc -O1 optimization. Parameters: number of ways (rep. policy), lines,
    words per line.}
  \label{table:performance_fpga_ssrv}
  \centering
  \begin{tabular}{||r|r|r||r|r|}
    \hline \textbf{L1-Instr} & \textbf{L1-Data} & \textbf{L2-Unified} &\textbf{clock cycles} & \textbf{CPI} \\\hline
    \multicolumn{5}{|c|}{\textbf{48 B - Minimum size}}\\\hline
    1, 2, 2 & 1, 2, 2 & 1, 2, 2 & 513926 & 12.971\\\hline
    \multicolumn{5}{|c|}{\textbf{2 KB}}\\\hline
    2 (PLRUt), 8, 8 & 2 (PLRUt), 8, 8 & 4 (PLRUm), 8, 8 & 185163 & 4.673\\\hline
    \multicolumn{5}{|c|}{\textbf{8 KB}}\\\hline
    2 (PLRUt), 16, 16 & 2 (PLRUt), 16, 16 & 4 (PLRUm), 16, 16 & 51298 & 1.294\\\hline 
    \multicolumn{5}{|c|}{\textbf{32 KB}}\\\hline
    2 (PLRUt), 64, 16 & 2 (PLRUt), 64, 16 & 4 (PLRUm), 64, 16 & 42397 & 1.070\\\hline
  \end{tabular}
\end{table}

In simulation, the main memory is modeled using an AXI4 RAM description. In the
FPGA the main memory is implemented with a DDR4 external module, with a longer
access time. The increased SDRAM's access time compared to simulation results in
an increase of 60.8\% (4.905 CPI) for the 48-Byte cache. Since it is the
smallest cache, its average access time is closest to the SDRAM's. With the 2 KB
cache, the access time only increases 14.2\% (0.581 CPI). With 8 KB the cache is
large enough to fit most of the program, and the FPGA times compared to
simulation are only 1.01\% higher (0.013 CPI).

\section{Synthesis}
\label{chapter:synthesis}

In this section, the cache's synthesis results are analyzed.  First the resource
utilization for each individual module is checked, followed by the resources
consumed by the entire cache.

The synthesis tool used is Xilinx's Vivado Design Suite 2017~\cite{bib:vivado},
with the \ac{FPGA} part "xcku040-fbva676-1-c" (AES KU040 Ultrascale). The
resources in analyses are: \ac{LUT}; \ac{LUTRAM}; \ac{FF}; and \ac{BRAM}.  For
the Block-RAM resources, there are 2 variants: RAMB18,
RAMB36~\cite[p.~7]{bib:ultrascale_memory}.

Despite being able to change the cache's word size with the parameter
FE\_DATA\_W, it was left to 32-bit, since the cache was only tested in 32-bit
systems.

The cache submodules are synthesized using a 100 MHz clock for the resources
presented in the next subsections. The entire cache is synthesized at 100 and
250 MHz clock frequency and respective resources presented.

\subsection{Front-End}
\label{chapter:resources_frontend}

The Front-End has two possible configurations, with or without the
Cache-Control's implementation. The results are represented in Table
\ref{table:resources_frontend}. The number of registers to store the input
signals: 32 bits for write-data, 28 bits for the address (word-addressable) and
1 for valid signal is the same with or without the Cache-Control module. With
the Cache-Control module, the area is essentially consumed by the multiplexers
implemented to select the read-data.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Front-End resources.}
  \label{table:resources_frontend}
  \centering
  \begin{tabular}{||r||r|r|}
    \hline \textbf{CTRL\_CACHE} & \textbf{LUT} & \textbf{FF} \\\hline 0 & 1 &
    67\\\hline 1 & 19 & 67\\\hline
  \end{tabular}
\end{table}



\subsection{Back-End}
\label{chapter:resources_backend}

For Back-End module, both the Read-Channel and Write-Channel modules are
analyzed individually. The Back-End is synthesized for data widths of 32 and 256
bits. Table~\ref{table:resources_writechannel} shows the Write-Channel's
resources for both Native and AXI interfaces. The 256-bit back-end adds
multiplexers to place the 32-bit words and 4-bit write-strobe in the back-end's
bus. The AXI bus type adds negligible resources compared to the Native type.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Back-End: Write-Channel's resources.}
  \label{table:resources_writechannel}
  \centering
  \begin{tabular}{||r|r||r|r|}
    \hline \textbf{FE\_DATA\_W} & \textbf{BE\_DATA\_W} & \textbf{LUT} &
    \textbf{FF} \\\hline
    \multicolumn{4}{|c|}{\textbf{Native-Interface}}\\\hline
    32 & 32 & 4 & 1\\\hline
    32 & 256 & 35 & 1\\\hline
    \multicolumn{4}{|c|}{\textbf{AXI-Interface}}\\\hline
    32 & 32 & 6 & 2\\\hline
    32 & 256 & 40 & 2\\\hline
  \end{tabular}
\end{table}


Table~\ref{table:resources_writechannel} represents Read-Channel's resources for
both Native and AXI interfaces. The amount of logic required depends strongly on
the width ratio between the main memory word and the cache line. The Back-End
Native-Interface requires an additional 15 LUTs to select which controller accesses
the main memory since both share the same bus.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Back-End Read-Channel's resources.}
  \label{table:resources_readchannel}
  \centering
  \begin{tabular}{||r|r|r||r|r|}
    \hline \textbf{FE\_DATA\_W} & \textbf{BE\_DATA\_W} & \textbf{Cache line
      Width} & \textbf{LUT} & \textbf{FF} \\\hline
    \multicolumn{5}{|c|}{\textbf{Native-Interface}}\\\hline
    32 & 32 & 64 & 4 & 3\\\hline
    32 & 32 & 128 & 5 & 4\\\hline
    32 & 32 & 256 & 7 & 5\\\hline
    32 & 64 & 64 & 4 & 2\\\hline
    32 & 64 & 128 & 4 & 3\\\hline
    32 & 64 & 256 & 5 & 4\\\hline
    32 & 256 & 256 & 4 & 2\\\hline
    32 & 256 & 512 & 4 & 3\\\hline
    32 & 256 & 1024 & 5 & 4\\\hline
    \multicolumn{5}{|c|}{\textbf{AXI-Interface}}\\\hline
    32 & 32 & 64 & 7 & 4\\\hline
    32 & 32 & 128 & 8 & 5\\\hline
    32 & 32 & 256 & 9 & 6\\\hline
    32 & 256 & 256 & 5 & 2\\\hline
    32 & 256 & 512 & 7 & 4\\\hline
    32 & 256 & 1024 & 8 & 5\\\hline
  \end{tabular}
\end{table}


\subsection{Resources: Cache-Control}
\label{chapter:resources_cachecontrol}

Cache-Control has 2 available implementations, with counters or without them.
In a bare Cache-Control implementation, the 3 LUTs and 3 FFs are the
memory-mapped registers, used to select the functions: invalidate, write-through
buffer empty and full. If implemented, each counter requires a 32-bit register.
The large increase in LUTs results from the arithmetic adders.  Each counter
requires incrementers, and 2 additional adders are required for the number of
cache-hits and cache-misses. With the counters, 7 Cache-Control functions are
available, requiring additional memory-mapped registers. The functions are:
retrieving the number of cache hits / misses / read-hits / read-misses /
write-hits / write-misses, and resetting the counters.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Cache-Control resources.}
  \label{table:resources_cachecontrol}
  \centering
  \begin{tabular}{||r|r||r|r|}
    \hline \textbf{CTRL\_CACHE} & \textbf{CTRL\_CNT} & \textbf{LUT} &
    \textbf{FF} \\\hline 1 & 0 & 3 & 3 \\\hline 1 & 1 & 266 & 163\\\hline
  \end{tabular}
\end{table}


\subsection{Replacement Policy}
\label{chapter:resources_replacementpolicy}

The Replacement Policy module is analyzed before the Cache-Memory module since
the former is implemented in the latter.  The results of the analysis are
available in Table~\ref{table:resources_replacementpolicy}.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Replacement Policy resources.}
  \label{table:resources_replacementpolicy}
  \centering
  \begin{tabular}{||l|r|r||r|r|}
    \hline \textbf{R.Policy} & \textbf{Ways} & \textbf{Lines} & \textbf{LUT} & \textbf{FF} \\\hline
    \multicolumn{5}{|c|}{\textbf{Single cache line}}\\\hline
    LRU & 2 & 1 & 3 & 2\\\hline
    PLRUm & 2 & 1 & 3 & 2\\\hline
    PLRUt & 2 & 1 & 3 & 1\\\hline
    
    LRU & 4 & 1 & 20 & 8\\\hline
    PLRUm & 4 & 1 & 8 & 4\\\hline
    PLRUt & 4 & 1 & 6 & 3\\\hline
    
    LRU & 8 & 1 & 81 & 24\\\hline   
    PLRUm & 8 & 1 & 22 & 8\\\hline
    PLRUt & 8 & 1 & 13 & 7\\\hline
    
    \multicolumn{5}{|c|}{\textbf{Multiple cache lines}}\\\hline
    LRU & 2 & 16 & 26 & 32\\\hline
    PLRUm & 2 & 16 & 26 & 32\\\hline
    PLRUt & 2 & 16 & 28 & 16\\\hline
    
    LRU & 4 & 16 & 70 & 128\\\hline
    PLRUm & 4 & 16 & 42 & 64\\\hline
    PLRUt & 4 & 16 & 33 & 48\\\hline
    
    LRU & 8 & 16 & 196 & 384\\\hline  
    PLRUm & 8 & 16 & 77 & 128\\\hline      
    PLRUt & 8 & 16 & 56 & 112\\\hline

    
    LRU & 2 & 128 & 222 & 256\\\hline
    PLRUm & 2 & 128 & 222 & 256\\\hline
    PLRUt & 2 & 128 & 212 & 128\\\hline

    
    LRU & 4 & 128 & 445 & 1024\\\hline
    PLRUm & 4 & 128 & 297 & 512\\\hline
    PLRUt & 4 & 128 & 259 & 384\\\hline
    
    LRU & 8 & 128 & 1076 & 3072\\\hline        
    PLRUm & 8 & 128 & 452 & 1024\\\hline       
    PLRUt & 8 & 128 & 403 & 896\\\hline
  \end{tabular}
\end{table}

The test is divided into 2 sections: single cache line and multiple cache lines.
It is not possible to synthesize the entire cache with a single cache line, so
this is only valid for the analysis of this module.

The single cache line results show how many LUTs are required to implement the
Policy Info Updater and Way Select Decoder. The number of FFs represents the
number of bits the Policy Info Memory module needs to store for each set.

The multiple cache lines results show the current actual amount of resources
required to implement each replacement policy. The current implementation of the
Policy Info Module (PIM) is register-based, so it requires additional logic
(LUTs) to address each set.  The number of LUTs is proportional to the total
number of bits in the PIM.

Since the LRU requires $N\_WAYS \times log_2(N\_WAYS)$ bits per set, initially
its size grows fast with the number of ways. In an 8-way set-associative cache
with 128 lines, the LRU requires more than twice the amount of LUTs and at least
thrice the amount of FFs compared with the PLRU policies. In a 2-way
set-associative cache, the replacement policies had the same performance but the
PLRUt's PIM requires half the number of FFs compared to the other two.


\subsection{Cache-Memory}
\label{chapter:resources_cachememory}

Cache-Memory is the module that contains the majority of the cache's resources.
It contains all the RAM memories and, if configured, the Replacement Policy
module too.  The synthesis results are available in
Table~\ref{table:resources_cachememory}.

\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Cache-Memory resources.}
  \label{table:resources_cachememory}
  \centering
  \begin{tabular}{||r|r|r|r||r|r|r|r|r|}
    \hline \textbf{Ways} & \textbf{R.Policy} & \textbf{Lines} &
    \textbf{Words/line} & \textbf{LUT} &\textbf{LUTRAM} & \textbf{FF} &
    \textbf{RAMB36} & \textbf{RAMB18}\\\hline
    \multicolumn{9}{|c|}{\textbf{1
        KB}}\\\hline
    1 & & 16 & 16 & 496 & 534 & 599 & 1 & 0\\\hline
    1 & & 64 & 4 & 292 & 128 & 239 & 1 & 1\\\hline
    1 & & 128 & 2 & 300 & 0 & 174 & 1 & 9 \\\hline
    \multicolumn{9}{|c|}{\textbf{2 KB}}\\\hline
    1 & & 32 & 16 & 548 & 533 & 614 & 1 & 0\\\hline
    1 & & 128 & 4 & 341 & 0 & 175 & 1 & 17 \\\hline
    2 & PLRUt & 16 & 16 & 950 & 1068 & 1167 & 1 & 0 \\\hline
    \multicolumn{9}{|c|}{\textbf{4 KB}}\\\hline
    1 & & 32 & 32 & 995 & 1044 & 1126 & 1 & 0\\\hline
    1 & & 128 & 8 & 405 & 0 & 176 & 1 & 33 \\\hline
    2 & PLRUt & 128 & 4 & 819 & 0 & 433 & 1 & 34 \\\hline
    \multicolumn{9}{|c|}{\textbf{8 KB}}\\\hline
    1 & & 128 & 16 & 551 & 0 & 177 & 1 & 65 \\\hline
    2 & LRU & 128 & 8 & 1037 & 0 & 562 & 1 & 66\\\hline
    2 & PLRUm & 128 & 8 & 1037 & 0 & 562 & 1 & 66\\\hline
    2 & PLRUt & 128 & 8 & 1003 & 0 & 434 & 1 & 66\\\hline
    \multicolumn{9}{|c|}{\textbf{16 KB}}\\\hline
    1 & & 128 & 32 & 957 & 0 & 178 & 1 & 129 \\\hline
    1 & & 512 & 8 & 933 & 0 & 560 & 1 & 33 \\\hline
    4 & LRU & 128 & 8 & 2055 & 0 & 1590 & 1 & 132\\\hline
    4 & PLRUm & 128 & 8 & 1913 & 0 & 1078 & 1 & 132\\\hline
    4 & PLRUt & 128 & 8 & 1877 & 0 & 950 & 1 & 132\\\hline
    4 & PLRUt & 64 & 16 & 2282 & 0 & 1845 & 1 & 68\\\hline
    \multicolumn{9}{|c|}{\textbf{32 KB}}\\\hline
    1 & & 128 & 64 & 1760 & 0 & 179 & 1 & 257 \\\hline
    1 & & 1024 & 8 & 1616 & 0 & 1072 & 1 & 33 \\\hline
    8 & LRU & 128 & 8 & 3935 & 0 & 4158 & 1 & 264\\\hline
    8 & PLRUm & 128 & 8 & 3341 & 0 & 2110 & 1 & 264\\\hline
    8 & PLRUt & 128 & 8 & 3293 & 0 & 1982 & 1 & 264\\\hline
  \end{tabular}
\end{table}

As mentioned in Section~\ref{chapter:cache_memory}, Data-Memory
infers a RAMB18 blocks for each byte in the cache line. Since the RAMB18 block
is 18-bit wide, roughly half of it is wasted.
%, or 36-bit if only half or less of its depth is used (512 or less).
This issue can be solved by describing a RAM
with multiple byte-enables as in the Vivado Synthesis
Guide~\cite{bib:ultrascale_memory} but unfortunately the description is
not portable for other FPGAs such as Intel's.

In the configurations with 128 lines or lower, the cache is implemented with
LUTRAMs plus output registers. With 128 lines or more, RAMB18 is used. RAMB36
blocks are never inferred because these have a 36-bit width. The Write-Through
Buffer, which is 64-bit wide, is implemented with LUTRAMs plus output registers
if its depth is 32 or lower, or is implemented with RAMB36 if the depth is
higher than 32. Note that RAMB36 blocks can be configured for 64-bit width and
RAMB18 blocks can not.

In general, looking at the results in Table~\ref{table:resources_cachememory},
the memory resources increase with both the width and depth of the cache memory,
although they increase by steps that have to do with the capacity of the FPGA
RAMB18 and RAMB36 memory resources. Increasing the number of ways, increases
everything, memory, and logic. The logic increases significantly to combine 
multiple ways and implement the Replacement Policy module.


\subsection{IOb-Cache}
\label{chapter:resources_iobcache}

In this section, the entire cache module is
analyzed. Table~\ref{table:resources_iobcache} displays the synthesis and timing
results of IOb-Cache using the Native interface for 2 different clock
frequencies: 100 and 250 MHz. The results for IOb-Cache with AXI Back-End are
similar and differ only in 15 LUTs and 2 \ac{FF}s.
%Data-Memory was implemented multiple byte-enables RAM.


\begin{table}[!h!tb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{IOb-Cache (Native) resource and timing analysis.}
  \label{table:resources_iobcache}
  \centering
  \begin{tabular}{||r|r|r|r||r|r|r|r|r|r|}
    \hline \textbf{Ways} & \textbf{R.Policy} & \textbf{Lines} &
    \textbf{Words/line} & \textbf{LUT} &\textbf{LUTRAM} & \textbf{FF} & \textbf{RAMB36} & \textbf{RAMB18} & \textbf{WNS}\\\hline
    \multicolumn{10}{|c|}{\textbf{100 MHz (10 ns)}}\\\hline
    %\multicolumn{10}{|c|}{\textbf{2 KB}}\\\hline
    %2 & PLRUt & 32 & 8 & 660 & 22 & 265 & 1 & 16 & 5.052\\\hline
    \multicolumn{10}{|c|}{\textbf{4 KB}}\\\hline
 1 & & 128 & 8 & 431 &0 & 249 &1&
    33 & 4.047\\\hline 4 & PLRUm & 16 & 16 & 1727 &1068 & 2407 &1& 0 &
    3.212\\\hline 1 & & 128 & 8 & 413 &0& 251 &1& 33 & \\\hline

    %1 & & 128 & 8 & 401 & 0 & 249 & 1 & 9 & 4.310\\\hline
    %2 & PLRUt & 128 & 4 & 836 & 0 & 505 & 1 & 10 & 3.535 \\\hline
    %4 & PLRUm & 32 & 8 & 1258 & 1068 & 2395 & 1 & 0 & 2.963\\\hline
    % 4 & PLRUm & 16 & 16 & 1258 & 1068 & 2395 & 1 & 0 & 2.963\\\hline
    \multicolumn{10}{|c|}{\textbf{8 KB}}\\\hline
 2 & PLRUt & 128 & 8 & 1025 & 0 &
    509 & 1&66 & 2.977\\\hline
    %2 & PLRUt & 128 & 8 & 975 & 0 & 509 & 1 & 18 & 3.018\\\hline
    \multicolumn{10}{|c|}{\textbf{16 KB}}\\\hline
4 &
    PLRUm & 128 & 8 & 1940 & 0 & 1154 & 1&132 & 2.158\\\hline
    % 4 & PLRUm & 128 & 8 & 1594 & 0 & 1155 & 1 & 36 & 2.712\\\hline
    \multicolumn{10}{|c|}{\textbf{32 KB}}\\\hline
4 & PLRUm & 256 & 8 & 2961 & 0
    & 2187 &1& 132 & 1.199\\\hline 1 & & 1024 & 8 & 1638 & 0 & 1145 & 1&33 &
    4.003\\\hline

    %   4 & PLRUm & 256 & 8 & 2587 & 0 & 2187 & 1 & 36 & 3.008\\\hline
 %   1 & & 1024 & 8 & 1601 & 0 & 1145 & 9 & 1 & 3.724\\\hline
    
    \multicolumn{10}{|c|}{\textbf{250 MHz (4 ns)}}\\\hline
    
   % \multicolumn{10}{|c|}{\textbf{2 KB}}\\\hline
   % 2 & PLRUt & 4 & 4 & 963 &534 & 1242 & 0 & 0.257\\\hline
    \multicolumn{10}{|c|}{\textbf{4 KB}}\\\hline
    1 & & 128 & 8 & 510 &40 & 269 &1&
    32 & 0.398\\\hline 4 & PLRUm & 16 & 16 & 1730 &1068 & 2407 & 1& 0 &
    0.024\\\hline
    
%    1 & & 128 & 8 & 479 & 40 & 269 & 1 & 8 & 0.645\\\hline
%    2 & PLRUt & 128 & 4 & 861 & 84 & 547 & 1 & 8 & 0.466\\\hline
%    4 & PLRUm & 16 & 16 & 1285 &1068 & 2394 & 1 & 0 & 0.113\\\hline
    \multicolumn{10}{|c|}{\textbf{8 KB}}\\\hline
2 & PLRUt & 128 &
    8 & 1084 & 80 & 549 & 1& 64 & 0.228\\\hline
%    2 & PLRUt & 128 & 8 & 1031 & 80 & 549 & 1 & 16 & 0.423\\\hline
    \multicolumn{10}{|c|}{\textbf{16 KB}}\\\hline
4 & PLRUm & 128 & 8 & 1974 & 160 & 1234 & 1& 128 &
    0.103\\\hline 
%    4 & PLRUm & 128 & 8 & 1748 & 160 & 1235 & 1 & 32 & 0.147\\\hline
    \multicolumn{10}{|c|}{\textbf{32 KB}}\\\hline
1 & & 1024 & 8 &
1714 & 272 & 1162 &1& 32 & 0.523\\\hline
4 & PLRUm & 256 & 8 & 2981 & 304 &
    2289 &1& 128 & -0.120\\\hline
%
    
%    1 & & 1024 & 8 & 1681 & 272 & 1162 & 9 & 0 & 0.552\\\hline
%    4 & PLRUm & 256 & 8 & 2780 & 304 & 2309 & 1 & 32 & -0.218\\\hline
  \end{tabular}
\end{table}

The implementation differs for the 2 clock frequencies. The used memory is
enough for BRAMs to be inferred for both the Tag and Data memories. For 100 MHz,
the critical-path is from Tag memory output to a Data memory write enable
signal. This path is caused by the signal way\_hit, which results from the tag
comparison and respective validation, and needs to be connected to write enable
bits on write-hit access. However, for 250 MHz the synthesis tool deliberately
decides to implement the Tag-Memory with \ac{LUTRAM}s, with a stage register at
the output, to be able to meet the timing constraint.


\clearpage
\section{Open-Source Caches}
\label{chapter:opensource}

In this chapter, the IOb-Cache is compared with the configurable PoC.cache
design included in the \ac{PoC}-Library~\cite{bib:poc} library of open-source
cores. PoC.cache is the most competitive open-source cache one was able to find,
so the other caches mentioned in Chapter~\ref{chapter:background} are not evaluated here
as clearly they cannot compete with IOb-Cache or PoC.cache. The comparison
between the two caches is available in Table~\ref{table:poc_vs_iob} below.

\begin{table}[!htb]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Comparison between PoC.cache and IOb-Cache.}
  \label{table:poc_vs_iob}
  \centering
  \begin{tabular}{||l||l|l|}
    \hline \textbf{} & \textbf{PoC.cache} & \textbf{IOb-Cache}\\\hline
    \textbf{HDL} & VHDL & Verilog\\\hline
    \multicolumn{3}{|c|}{\textbf{Configurability}}\\\hline \textbf{num. of ways}
    & Yes & Yes \\\hline \textbf{num. of lines} & Yes & Yes \\\hline
    \textbf{num. of words/line} & Yes & Yes \\\hline \textbf{back mem.'s width}
    & Yes & Yes \\\hline \multicolumn{3}{|c|}{\textbf{Mapping}}\\\hline
    \textbf{Direct} & Yes & Yes \\\hline \textbf{Set-Associative} & Yes & Yes
    \\\hline \textbf{Full-Associative} & Yes & No \\\hline
    \multicolumn{3}{|c|}{\textbf{Policies}}\\\hline \textbf{Write} &
    write-through not alloc. & write-through not alloc.\\\hline
    \textbf{W.Through Buffer} & {No} & {Yes} \\\hline \textbf{Replacement} & LRU
    & LRU, PLRUm, PLRUt \\\hline \multicolumn{3}{|c|}{\textbf{Back-End
        Connectivity}}\\\hline \textbf{Native} & Yes & Yes \\\hline \textbf{AXI}
    & No & AXI4 \\\hline \multicolumn{3}{|c|}{\textbf{Implementation}}\\\hline
    \textbf{Main-control} & FSM & Data-path \\\hline \textbf{Data-Memory} & BRAM
    & BRAM \\\hline \textbf{Tag-Memory} & LUTRAM & BRAM \\\hline
    \textbf{Valid-Memory} & Register & Register \\\hline \textbf{Rep-Pol. Mem} &
    Register & Register \\\hline \textbf{Invalidate} & Yes & Yes \\\hline
    \textbf{Inval. source} & input port & input port + aux.module\\\hline
    \multicolumn{3}{|c|}{\textbf{Performance (best case scenario)}}\\\hline
    \textbf{clk/read (hit)} & 1 & 1 \\\hline \textbf{clk/write} & 2 & 1 \\\hline
    \textbf{Ready assertion} & same cycle as valid req. & cycle after valid
    req.\\\hline \textbf{Read-Data availability} & cycle after ready & same
    cycle as ready\\\hline \textbf{New Valid req.} & cycle after ready & same
    cycle as ready\\\hline
  \end{tabular}
\end{table}


In addition to the information in Table~\ref{table:poc_vs_iob}, the following
remarks are needed. The data-width of the back-end of PoC.cache is fixed to the
cache line's size, and therefore not configurable to be smaller such as in
IOb-Cache. The PoC.cache tag and valid memories are implemented with distributed
LUTRAM and registers, respectively, to combinatorially check for a hit and
achieve 1 read per clock cycle. Lastly, despite using the Write-Though policy,
PoC.cache does not have a buffer and accesses the main memory for write
transfers, which is comparatively slower.

Based on the information in Table~\ref{table:poc_vs_iob}, the following
conclusions can be drawn. There are 2 points where PoC.cache is better than
IOb-cache: (1) the implementation of the cache invalidate function and (2) the
implementation of a fully associative cache. PoC.cache is able to invalidate
individual lines whereas IOb-Cache can only invalidate the entire
cache. PoC.cache can be configured as a fully-associative (single set) cache and
IOb-Cache needs at least 2 sets. However, besides its theoretical interest
fully-associative caches are seldom used in practice.

In the remaining features, IOb-Cache is better than PoC.cache: configurable
back-end size with AXI4 interface as an option; write-through buffer and
independent controller for fast, most of the time 1-cycle writing (PoC.cache
only supports 1-cycle reads); more replacement policies to choose from; a
modular design that allows changing both front and back-ends without affecting
the cache's core functionality.

Both PoC.cache and IOb-cache have the same issue of implementing the Tag-Memory
and Policy Info Module using registers, and thus consuming more silicon area
than necessary. However, because IOb-Cache is designed to work with the 1-cycle
read latency of RAM, it can easily be upgraded to replace these memories with
RAMs while PoC.cache needs more drastic design changes.



