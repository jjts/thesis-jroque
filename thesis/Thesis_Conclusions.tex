\chapter{Conclusions}
\label{chapter:conclusions}

In this thesis IOb-Cache, a high-performance configurable open-source cache is
developed. IOb-Cache is being used in dozens of projects. It is currently
integrated into the IOb-SoC Github repository, which has 16 stars and is being
used in 38 projects (forks). In the Github cloud community, it is currently the
only Verilog cache found by its search tool, with this level of configurability,
that supports pipelined CPU architectures, and the popular AXI4 bus interface.

The cache is composed of 3 modules: Front-End, Cache-Memory, and Back-End.  The
Front-End interfaces the processor with the cache. The Cache-Memory contains the
memories and the cache's main controller. The main controller is implemented by
a streamlined datapath that evaluates every necessary condition for read and
write accesses. The Back-End implements Native and AXI interfaces, allowing
flexibility in connecting to 3rd party memory controllers (likely using an AXI
interface), and other cache levels (likely using the Native interface). The
The native interface follows a pipelined valid-ready protocol, while the AXI4
interface is a full master bus implementation.  Each interface has a specific
controller for write and read accesses. The Back-End's Write-Channel module is
responsible for the write accesses: it reads data from the write-through buffer
and writes them to the main memory. The Back-End's Read-Channel module fetches
lines from the main memory and writes them to the cache during a cache line
replacement. An optional module called Cache-Control can be selected. This
module implements cache performance meters, and analyses write-through buffer
states and cache invalidates. These functions are controlled by the CPU using
memory-mapped registers. When the Cache-Control module is present, the Front-End
module acts as a splitter between accesses to Cache-Memory or the Cache-Control
modules, also through memory-mapping.

In the remainder of this chapter the main achievements of this work and the main
directions for future work are outlined.

% ----------------------------------------------------------------------
\section{Achievements}
\label{section:achievements}

In this work, several important achievements deserve to be highlighted. The
the following list highlights them.

\begin{itemize}

\item The cache supports pipelined memory loads and stores, honoring 1 request per clock cycle. The available configurations revolve around the cache's
  dimensions, replacement policies, and back-end memory interface
  data-width. Given the 1-cycle latency present in RAMs, a request can be served
  while processing the next, which results in a throughput of 1 request per
  clock-cycle and latency of 2 clock cycles for hit addresses.

\item The cache has a modular design that allows keeping its core
  functionality independent from its interfaces intact, implemented by the Front-End and
  Back-End modules.

\item The cache is able to pass high frequency (250 MHz) timing requirements for
  a Xilinx Ultrascale FPGA, in a large number of configurations, including the 32
  KB direct-mapped or the 8KB 4-way set-associative configurations.

\item If large enough, the results show that its performance is close to that of having a fast on-chip RAM connected to the CPU. Using the multi-issue
  superscalar SSRV CPU, which has CPI=1.02 when connected to a RAM, the cache
  achieved CPI=1.07.

\end{itemize}




% ----------------------------------------------------------------------
\section{Future Work}
\label{section:future}

IOb-Cache can still be further improved beyond the development time allocated to
it. The main improvements are listed below in decreasing priority:

\begin{itemize}

\item Increase resource efficiency by reducing the amount of logic and memory used. Both the Valid-Memory and PIM modules would be more efficiently
  implemented with RAM; the Valid Memory could be merged with the Tag-Memory
  adding only 1 bit to its width. This would eliminate many registers and logic but the ability to invalidate the cache in a single cycle would be lost, as each line would have to be accessed for invalidation using a hardware or software scheme. Implementing the back-end controllers with logic datapaths
  instead of FSMs is not expected to further reduce the resources used but it
  would definitely improve the code's readability.
  
\item Implementation of the Write-Back Write-Allocate policy. Currently, only the write-Through policy is supported, which limits the write-bus capacity to the cache's word width and uses more bandwidth of the memory controller, blocking other devices they may need to access the external memory. Using the write-back policy it is possible to optimize this bandwidth and the general performance for some applications. Ideally, these two policies should be
  configurable.

\item Improve the Cache-Control module. The current Cache-Control module is very crude, especially the invalidate function. It should be possible to invalidate a single selected cache line, which would require an address decoder. Alternatively, merging the Valid memory with the Tag memory as proposed before solves this problem. Moreover, if the write-back policy is
  implemented, it will need a line flush function, which is automatically
  guaranteed by the ability to individually select lines for invalidation.

\item Cache Coherency. The current cache is weak for multi-processor systems since it lacks a coherency controller. A dedicated module and interface to
  implement a cache coherency algorithm would significantly expand the usability
  of IOb-Cache.
  
\end{itemize}
