\chapter{System}
\label{chapter:system}

This chapter contains a brief introduction to the system where IOb-Cache was
implemented, and presents a multi-level cache implementation with multiple
instances of IOb-Cache.

\section{IOb-SoC}
\label{chapter:iob_soc}

IOb-Cache has been integrated in IOb-SoC~\cite{bib:iob_soc}, an open-source
synthesizable system developed by IObundle in Verilog. Its design can be seen in
Figure~\ref{fig:iob_soc}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\linewidth]{Figures/iob_soc}
  \caption{IOb-SoC module diagram.}
  \label{fig:iob_soc}
\end{figure}

The system is designed to allow the integration of multiple user peripherals,
accessed through memory-mapping. Each individual peripheral device is given a
specific address range to be accessed. The main peripherals required for a
functional bare-metal system will be briefly described.

The interconnect is implemented with "split"~\cite{bib:iob_interconnect} units,
which is the module responsible for connecting the processor (master) to the
remaining peripherals (slaves). The connection is established through
memory-mapping, where the \ac{MSB} or the MSB-1 bit of the address selects all
peripherals, depending on whether a secondary memory is not present or present
in the system, respectively.

This system is controlled by a RISC-V processor. A CPU wrapper converts the CPU
interface signals to the Native interface used internally throughout the system
for interconnecting the different modules. The wrapper has the necessary
combinational and sequential logic for implementing this interface. Currently, a
simple 2-stage machine (PicoRV32~\citep{bib:picorv32,bib:iob_pico}), or a more
complex super-scalar multi-issue processor (SSRV~\citep{bib:ssrv,bib:iob_ssrv})
are supported.

For communications between the system and the host, a UART module
(IOb-UART~\cite{bib:iob_uart}) is integrated. It uses the Universal Asynchronous
Receiver / Transmitter protocol (UART) for transmitting and receiving serial
data.

An SRAM memory and a Boot \ac{ROM} memory are integrated into a module called Internal Memory, which also contains a soft reset mechanism for transitioning
from the bootloader to the main program and vice-versa.

The Boot ROM memory contains a program that runs the boot sequence and runs
when the system is reset. This program loads the firmware into the main memory
while receiving it from the host using the UART. After the firmware is loaded,
the system is soft-reset, which causes the memory map to be restructured. After
soft reset the system restarts but, instead of running the bootloader program,
it starts fetching instructions from the main memory where the firmware had been
previously loaded, specifically at address 0.  When the soft reset register is
accessed it toggles a control register which is used to alter the system's
memory-map. After running the firmware, this register is retoggled and a new
soft reset is issued, causing the system to reboot and run the bootloader again
to load the firmware, which thus can be changed without recompiling the
hardware.

External Memory module allows access to an external and larger DRAM memory
(DDR3 or DDR4), and is where the IOb-Cache modules are placed. External Memory module connects the system to an external DDR memory soft
controller provided by the FPGA vendor and using the AXI4 interface. This
explains why AXI4 interfaces have been implemented for the cache back-end.


The IOb-SoC system has been implemented in an XCKU040-1FBVA676
\ac{FPGA}~\cite{bib:ku040}, which is part of the Xilinx’s Ultrascale \ac{FPGA}
family. A diagram of the FPGA system and board is shown in
Figure~\ref{fig:iob_soc_fpga}.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\linewidth]{Figures/iob_soc_fpga}
  \caption{IOb-SoC FPGA implementation module diagram.}
  \label{fig:iob_soc_fpga}
\end{figure}

This board features a 250MHz system clock oscillator and a 1 GB DDR4 \ac{SDRAM}
memory module. The memory controller \ac{PHY} is implemented using Xilinx's
Memory Interface Generator (MIG) software which generates memory controller IP
cores~\cite{bib:MIG} from user parameters. The controller is implemented
with the widely adopted AXI4 interface which connects to the cache's back-end.

The memory controller clock has 1/4 of the frequency at which the memory
\ac{PHY} operates. The range of available frequencies for the DDR4 is between
625 and 933 MHz. This limits the range of clock frequencies for the memory
controller and system if connected using the same clock, to be between 156.25
and 233.25 MHZ.  In the current design, the controller is running at 200 MHz,
which means the DDR4 runs at 800 MHz.

The DDR4 module has a bandwidth of x32@1600Mbps, so it would be able to exchange
32-bit words with the system at a frequency of 1600 MHz, but this frequency is
too high for an FPGA to achieve. Because the controller is only running at 200
MHz, the AXI4 data bus width must be increased proportionally to the desired
memory bandwidth. With a width of 256 bits, the full memory bandwidth can be put
to use.  The external memory and cache back-end bus widths need to be configured
for 256 bits for this to happen.

The system may not be set to operate at the controller's frequency range, so
connecting it to the controller requires an asynchronous 1-to-1 AXI-Interconnect
IP is also supplied by Xilinx. However, the Xilinx asynchronous interconnect IP
introduces a high latency, so it is best to choose an integer clock ratio and
use the synchronous version.


\section{IOb-Cache: Multi-Level configuration}
\label{chapter:iobcache_multilevel}

IOb-Cache modules can be connected to each other to form multi-level cache
systems.  A two-level cache system, composed of an L1-Instruction cache, an
L1-Data cache, both connected to a larger L2-cache is represented in
Figure~\ref{fig:ext_mem}. The two L1 caches access different types of data, one
accesses instructions, and the other accesses data. The L2 cache merges the
accesses of the instruction and data caches and thus may contain both
instructions and data.


\begin{figure}[h]
  \centering
  \includegraphics[width=0.7\linewidth]{Figures/multi_level_cache}
  \caption{External Memory: two-level cache system implementation.}
  \label{fig:ext_mem}
\end{figure}

The back-end of the L1 instruction and data caches use the Native Interface and
are connected to a 2-to-1 interconnect called
"merge"~\cite{bib:iob_interconnect}. The merge unit connects several masters to
a slave interface using a fixed and sequential priority encoder. A master
remains connected to a slave until the request is acknowledged. The back-end of
the merge block is connected to the front-end of the L2 cache which also uses
the Native interface. The L2 back-end uses the \ac{AXI4} interface and is
connected to the memory controller.

The Cache-Control optional module can only be implemented in the L1-Data cache
since it is the only cache directly accessed by the processor, and the
instruction L1 cache does not need one. To access the L2-cache, either for a
cache invalidation or checking of the status of the write-through buffer, the
CTRL\_IO pins are used instead. The CRL\_IO interface supports multi-cache
systems, so accessing the Cache-Control module for status verification, shows
the status of the downstream connected caches.  This is necessary during the
IOb-SoC booting procedure, to check if the entire firmware has already been
written to the main memory before restarting the system to run it.
