\section{Front-End}
\label{chapter:front_end}

The Front-End module interfaces the processor (master) and cache (slave). In the
current design, it splits the processor bus to access the cache memory itself or
the Cache-Control module (if present). It also registers some bus signals needed
by the cache memory. Its interface is presented in Table~\ref{table:front_end}
and Figure~\ref{fig:front_end} details the internal structure.

\begin{table}[h!]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Front-End ports}
  \label{table:front_end}
  \centering
  \begin{tabular}{||l||>{\raggedleft}p{2.5cm}|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\\hline
    \hline
    \textbf{valid}  & 1  & input  & Validates the request. \\\hline
    
    \textbf{addr} & FE\_ADDR\_W +(CTRL\_CACHE) & input  & Address signal, defines the requested location. When the Cache-Control is implemented , its width will increase by 1, the MSB used to access it.\\\hline
    
    \textbf{wdata} & FE\_DATA\_W   & input & Write-data, data to be stored in cache (if available (hit)) and in main memory (write-through). \\\hline
    
    \textbf{wstrb}   & FE\_NBYTES  & input  & Write-Strobe, validates each respective byte of write-data. Signal is 0 (all bits) during read-accesses.\\\hline
    
    \textbf{rdata}   & FE\_DATA\_W & output  & Read-Data, the requested data from either Cache-Memory or Cache-Control (only if implemented).\\\hline

    \textbf{ready}   & 1   & output  & The acknowledge signal that validates the conclusion of the request.\\\hline
    \multicolumn{4}{|c|}{\textbf{Cache-Memory}}\\\hline
    \textbf{data\_valid}      & 1                         & output & Validates the Cache-Memory's access.\\\hline
    
    \textbf{data\_valid\_reg} & 1                         & output & Registered 'data\_valid' signal.\\\hline

    \textbf{data\_addr}       & FE\_ADDR\_W -FE\_BYTE\_W & output & Main-memory address.\\\hline 

    \textbf{data\_addr\_reg}  & FE\_ADDR\_W -FE\_BYTE\_W & output & Registered 'data\_addr'.\\\hline

    \textbf{data\_wdata\_reg} & FE\_DATA\_W               & output & Write-Data registered signal. \\\hline

    \textbf{data\_wstrb\_reg} & FE\_NBYTES                & output & Write-Strobe registered signal.\\\hline
    
    \textbf{data\_rdata}      & FE\_DATA\_W               & input  & Cache-Memory's read-data.\\\hline

    \textbf{data\_ready}      & 1                         & input  & Cache-Memory's ready signal. Validates the conclusion of the access.\\\hline
    \multicolumn{4}{|c|}{\textbf{Cache-Control (optional)}}\\\hline
    \textbf{ctrl\_valid}      & 1                         & output & Validates the Cache-Control access. Only if Cache-Control is implemented. \\\hline

    \textbf{ctrl\_addr}       & CTRL\_ADDR\_W             & output & Selects the Cache-Control's task. \\\hline

    \textbf{ctrl\_rdata}      & FE\_DATA\_W               & input  & Cache-Control's requested read data (counters or buffer status).\\\hline

    \textbf{ctrl\_ready}      & 1                         & input  & Validates the conclusion of the Cache-Control's requested task.\\\hline
  \end{tabular}
\end{table}


The cache requires that during a request (valid), the master's inputs are
maintained until the cache signals its conclusion by asserting the ready signal.
During the assertion of the ready, a new access can be requested.

The signals required for memory writing and the ready's
combinational path are registered.  This way, the necessary input data is still
available while ready is asserted.

The cache always returns entire words since it is word-aligned.  This means the
access is word-addressable, so the byte-offset of the CPU address signal (last
FE\_BYTE\_W bits) is not connected to the cache.

In a system with a different CPU interface, only this module requires modification
to allow compatibility.

\begin{figure}[h]
  \centering
  \includegraphics[width=0.6\linewidth]{Figures/front_end}
  \caption{Front-End module diagram.}
  \label{fig:front_end}
\end{figure}

If the optional Cache-Control is implemented, this module also works as a
memory-map decoder to select which unit is being accessed, the memory or the
control unit.

This mapping is done using the \ac{MSB} of the port "addr". When high (1), the
Cache-Control module is accessed.  This also required some additional logic to
select which read data is sent to the master. This logic is a word-sized
multiplexer (read-data) and an OR-gate (valid).
