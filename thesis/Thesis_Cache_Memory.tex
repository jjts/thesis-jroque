\section{Cache-Memory}
\label{chapter:cache_memory}

Cache-Memory is a module that contains the cache's main controller and memories.
The available memories are the Tag memory, the Valid memory, the Data memory,
the write-through Buffer, and, if applicable, the Replacement-Policy memory.

Its main controller accesses specific back-end modules using a handshake
valid-ready approach. The ready-signal is always asserted excepts after valid
is asserted, where it becomes 0 until the request's conclusion.

Depending on the choice of parameters, Table~\ref{table:parameters}, the cache's implementation will either be
direct-mapped or set-associative based on the number of ways given by
N\_WAYS.

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=\linewidth]{Figures/cache_memory}
  \caption{Cache-Memory module diagram.}
  \label{fig:cache_memory}
\end{figure}

Table~\ref{table:cache_memory} describes the Cache-Memory's ports.
For simplicity, the Front-End signals' prefix, "data\_" was removed.

\begin{table}[h]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Cache-Memory ports.}
  \label{table:cache_memory}
  \centering
  \begin{tabular}{||l||>{\raggedleft}p{2.5cm}|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\
    \hline
     \multicolumn{4}{|c|}{\textbf{Front-End ports}}\\\hline
    \textbf{data\_valid}  & 1  & input & Valid signal, requests the access to Cache-Memory.\\\hline
    
    \textbf{data\_valid\_reg}  & 1  & input & Registered valid signal, required for memory writes.\\\hline
    
    \textbf{data\_addr} & FE\_ADDR\_W -FE\_BYTE\_W & input & Address signal of the word. Indexes the cache lines.\\\hline
    
    \textbf{data\_addr\_reg}   & FE\_ADDR\_W -FE\_BYTE\_W & input & Registered address signal. Required for memory writes, tag comparison, and  Data-Memory's word-selection.\\\hline
    
    \textbf{data\_wdata\_reg} & FE\_DATA\_W   & input & Registered write-data signal. Required for Data-Memory and Write-Through Buffer. \\\hline
    
    \textbf{data\_wstrb\_reg}   & FE\_NBYTES  & input &  Registered write-strobe signal. Write-data's byte-enable.\\\hline
    
    \textbf{data\_rdata} & FE\_DATA\_W & output & Read-data signal. Requested Data-Memory's word.\\\hline

    \textbf{data\_ready} & 1 & output & Ready signal. Acknowledges the conclusion of the request. Read-data is available (read-access). \\\hline
    \multicolumn{4}{|c|}{\textbf{Back-End: Write-Channel ports}}\\\hline
    \textbf{write\_valid} & 1 & input & Initiates write request to main memory. Write-Through Buffer's empty signal.\\\hline
    \textbf{write\_addr} & FE\_ADDR\_W -FE\_BYTE\_W & output & Address of the write request, stored in Write-Through Buffer.\\\hline
    \textbf{write\_wdata} & FE\_DATA\_W & output &  Write-data, stored in Write-Through Buffer.\\\hline
    \textbf{write\_wstrb} & FE\_NBYTES & output & Write-strobe (byte-enables), stored in Write-Through Buffer.\\\hline
    \textbf{write\_ready} & 1 & output & Reads Write-Through Buffer, data is available in its output next clock cycle.\\\hline
    
    \multicolumn{4}{|c|}{\textbf{Back-End: Read-Channel ports}}\\\hline
    \textbf{replace\_valid} & 1 & output &  Requests replacement of the cache line. Asserted during a read-miss.\\\hline
    \textbf{replace\_addr} & FE\_ADDR\_W -FE\_BYTE\_W -LINE2MEM\_W & output &  Memory block's base address to replacement a cache line. \\\hline
    \textbf{replace} & 1 & input & Signals that a line replacement is in progress.\\\hline
    \textbf{read\_valid} & 1 & input & Validates the memory block's word. Only during line replacement. \\\hline
    \textbf{read\_addr} & LINE2MEM\_W & input & Addresses the current position in the cache line. \\\hline
    \textbf{read\_data} & BE\_DATA\_W & input & Main memory's read-data, to be stored in the selected cache line, during the replacement.\\\hline
  \end{tabular}
\end{table}


Table~\ref{table:cache_memory_control} contains the additional ports used if the
module Cache-Control is implemented.

\begin{table}[h]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Cache-Memory ports: Cache-Control}
  \label{table:cache_memory_control}
  \begin{tabular}{||l||>{\raggedleft}p{2.5cm}|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\
    \hline
    \textbf{invalidate} & 1 & input & Invalidates entire cache, reseting Valid-Memory, and if applicable, the replacement-policy's memory.\\\hline
    \textbf{wtbuf\_empty} & 1 & output & Write-through buffer's empty signal, asserts when empty.\\\hline
    \textbf{wtbuf\_full} & 1 & output &  Write-through buffer's full signal, asserts when full.\\\hline
    \textbf{write\_hit} & 1 & output &  Write-hit, asserts when a write-request is available in the cache (hit).\\\hline
    \textbf{write\_miss} & 1 & output & Write-miss, asserts when a write-request is not available in the cache (miss). \\\hline
    \textbf{read\_hit} & 1 & output &  Read-hit, asserts when the requested data is available in the cache (hit).\\\hline
    \textbf{read\_miss} & 1 & output & Read-miss, asserts when the requested data is available in the cache (hit).\\\hline
  \end{tabular}
\end{table}

Before the Cache-Memory's behavior and design description is presented, the
implementation of each memory will be explained.

Figure~\ref{fig:tag_valid_memories} shows the Tag and Valid memories'
implementation, while Figure~\ref{fig:data_memory} the shows the Data memory.

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=\linewidth]{Figures/tag_valid_memories}
  \caption{Tag and Valid-Memories.}
  \label{fig:tag_valid_memories}
\end{figure}

The Tag memory is inferred using RAM. There is one Tag memory per cache
way. Each of these has Tag-sized width, and depth equal to the total number of
cache lines.

The Valid memory is composed of an array of 1-bit registers (register-file), one
for each way. Each array's length equals the number of cache lines. This choice
of implementation was a simple design choice to set its contents to 0 during
either a system reset or a cache-invalidate.

The Tag memory has a 1 clock-cycle read latency (\ac{RAM}), therefore the valid
memory's output signal needs to be delayed, either by applying a 1-bit output
stage-register or using the registered address ``data\_addr\_reg''. The latter was
chosen because it requires less logic and has no impact on timing. Both Tag and
Valid memories' outputs connect to comparators for producing hit/miss results
required for memory accesses.


\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=0.8\linewidth]{Figures/data_memory}
  \caption{Data-Memory.}
  \label{fig:data_memory}
\end{figure}

The Data memory is implemented by one \ac{RAM} for each way and (word) offset. 
Each \ac{RAM} has a width FE\_DATA\_W (cache word-size) and a depth of $2^{LINE\_OFF\_W}$ (number of cache lines). Since the
write-strobe signal selects which bytes are stored, each \ac{RAM} requires a
write enable for each byte.  Some synthesis tools can only infer single-enable
\ac{RAM}s~\citep{bib:altera_recom,bib:quartus_design_recom}, therefore a
\ac{RAM} will be inferred for each byte.

The Write-Though Buffer is implemented using a synchronous
FIFO~\cite{bib:iob_mem}. It requires the data to be available on its output a
clock-cycle after being read.

To address the words in the cache's memories, the input address signals are
segmented as described in Figure~\ref{fig:cache_memory_addr_segmentation}.

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=\linewidth]{Figures/cache_memory_addr_segmentation}
  \caption{Address signal segmentation.}
  \label{fig:cache_memory_addr_segmentation}
\end{figure}

The address (data\_addr) is only used for the initial addressing (indexing) of the
main memories: Valid, Tag, and Data.  On the next clock cycle, the registered address
(data\_addr\_reg) will be checked to see if a "hit" occurred and identifying the word within the cache line.

The hit check uses the signal "way\_hit".  Each of its bits indicates a hit in
the respective way. The hit is the result of a tag match.

If any bit of the "data\_wstrb\_reg" signal is enabled, it is a write-request, otherwise
it is a read-request.

During a read-request, if a hit is produced, the respective word is already
available in the Data-Memory's output, so the request can be acknowledged.

The Data memory allows input Data from both the Front and the Back-End. This
selection is done using the signal "replace", which indicates if the replacement
on a cache line is in action. While ``replace'' is not asserted, all accesses
are from the Front-End. During a read-miss, the signal ``replace'' is asserted,
which will start the Back-End Read-Channel controller, responsible for
line replacement.

Both Tag and Valid memories are updated when the ``replace\_valid'' signal is
high, forcing a hit in the selected way. This allows the replacement process to act
similarly to a regular write hit access, reducing the necessary logic. The
replacement can only start if there are not currently write transfers to the
main-memory.

%
% cannot proceed from here
%

The signals "write\_valid" and "write\_ready" constitute a handshaking pair for
Cache-Memory to write to the Back-End Write-Channel. The former indicates the
Write-Through Buffer is not empty, validating the transfer. The latter indicates
that the Back-End Write-Channel is idle and thus enables reading the
Write-Through Buffer.

The requirement that the replacement only starts after the write transfer is to
avoid coherency issues, i.e. storing outdated data in the cache-line.

Write requests do not depend on the data being available in the cache, since it
follows the write-not-allocate policy. Instead, it depends on the available
space in the Write-Through Buffer, which stores the address, write-data, and
write-strobe array.

During a write-hit, to avoid stalling, the Data memory uses the registered input
signals to store the data, so the cache can receive a new request.

If a read follows a write-access, \ac{RAW} hazards can become an issue. The
requested word may not be available at the memory output, since it was written
just the cycle before. This word will only be available in the following
clock-cycle, therefore the cache needs to stall.

Stalling on every read-request that follows a write-hit access can become costly
performance-wise. Hence, to avoid this cost a simple technique has been
employed: the cache stalls only if one wants to read from the same way and (word) offset that has
been written before. This results in RAW only signaling when the same Data memory's (byte-wide) RAMs are being accessed.

All the above conditions are implemented in the main controller circuit. The
signals data\_ready and hit are given combinatorially by
Equation~\ref{eq:cache_ready}.

\begin{equation}
  \!
  \begin{aligned}
    \text{data\_ready = (write\_access AND !buffer\_full) OR (read\_access AND hit AND !RAW)}\\
    \text{hit = OR(way\_hit) AND !replace}
    \end{aligned}
  \label{eq:cache_ready}
\end{equation}

A stall occurs if "data\_ready" is 0 after the request, otherwise the request is
acknowledged.

Because a new request can be issued in the same cycle as the previous is
acknowledged, the registered request signals are used for the tag comparison,
memory-writing and word-selection.

Since the line replacement controller uses the way\_hit signal, the hit signal
is 1 only when signal replace is de-asserted, as this signal already has a delay
to compensate for the Data memory's RAM 1 clock cycle read-latency.
