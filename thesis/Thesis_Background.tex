\chapter{Background}
\label{chapter:background}

In this chapter the existing state-of-the-art on open-source cache systems is
sketched and the theory of cache systems is briefly introduced. Being the
the central problem that this thesis solves the difficulty in finding a
comprehensive cache design to accompany the growing trend of open-source
hardware, the author set off to investigate the existing ones, which are
described in the first of the two sections of this chapter. The second section
provides an overview of cache systems, describing structure and implementation
policies.

\section{Open-Source Caches}
\label{chapter:cache}

In search of HDL open-source cache designs, the most relevant ones are found on
the Github platform. The caches on Github are chosen based on their popularity
(stars and forks): airin711's Verilog-caches~\cite{bib:airin711}; prasadp4009's
2-way-Set-Associative-Cache-Controller~\cite{bib:prasadp4009}; and PoC.cache,
which is part of the \ac{PoC}-Library~\cite{bib:poc}, one of the most popular
HDL libraries.

The airin711's Verilog-caches repository houses 3 different set-associative
caches: 4-way with \ac{LRU} replacement policy; 8-way with \ac{PLRU} and a
run-time configurable 2-to-8-way with PLRU replacement policy. All caches have 4
words per line and only allow configuring the number of lines.

The prasadp4009's Verilog cache repository is a 2-way set-associative cache that
uses the LRU replacement policy.  Unlike the airin771's caches, it allows
configuration of the number of cache lines and words per line, as well as the
width of both address and data.

Both caches the airin711's and prasadp4009's caches use write-back
write-allocate policy, native memory interface and are unable to either
invalidate or flush a cache line. The biggest difference between the two is the
fact that airin711's caches require the data memory to be 128-bit wide so that
the entire line or memory block can be accessed in a single word transfer. The
prasadp4009's cache requires the data memory width to be word-sized, using a
counter to receive the memory block or transfer the cache line.

Unfortunately, there is a big issue, which makes these two caches poor choices
with more advanced architectures: they need at least 2 clock cycles to process
requests, even if the data is already in the cache. These caches implement a \ac{FSM}
that controls all their functions, including the communication with the
processor and to the main memory. They only allow requests when their FSM is in
the initial state, and have special states for the read and write accesses
because of the RAM's 1 cycle read latency upon a cache hit. In the read or write
access, if a hit occurs, they acknowledge the request and go back to the initial
state.  This causes the undesirable 1 clock-cycle latency.


The third cache that was investigated,
PoC.cache~\cite{bib:poc}, does allow 1 read-access per clock
cycle, which is a big improvement over the others. It is also highly
configurable, with various synthesis parameters that characterize the cache
dimensions, even allowing 3 types of mapping: direct, set-associative, and
full-associative. It uses the LRU replacement policy, an effective but costly
policy, especially when compared to others that are cheaper but closely
effective. Despite only having a native memory interface, it has access to
adapters for other commonly used memories, especially SDRAMs.

The disadvantages of PoC.cache are described in this paragraph. PoC.cache uses a
control FSM, which during a read-access, only changes state on a miss. This
requires the hit to be checked in the same cycle a request is made. The tag and
valid memories are therefore implemented with distributed RAM and registers,
respectively. In the presence of a hit, PoC.cache acknowledges the request, but
the data is only available in the next clock cycle. In the following clock
cycle, when the data is available, it can receive a new request. This allows the
cache to operate with the 1 read per clock cycle. Write accesses on the other
hand require a change of state in the FSM, resulting in a minimum of 1 write per 2
clock cycles. The cache uses a write-through write-not-allocate policy but does
not have a write-through buffer. Instead, it accesses the main memory
directly. This means each write-access is dependent on the write-access time of
the memory interface controller, which is a big issue given that the
write-through policy is expected to generate significant traffic.

Despite being highly configurable, its main memory interface is limited to the
size of the cache line. The cache expects to load a line in a single transfer,
meaning the memory's data-width needs to be line-sized. This is not a negative
point since it maximizes the main memory's bandwidth, but may limit the memory
options. One severe limitation is when implementing a multi-level cache, as the
higher-level cache needs to have a word-size of the lower-level's line
width. The lack of a write-through buffer is also a big limitation since this
cache needs to stall during a write-access while the higher-level cache is
fulfilling another request.

PoC.cache is written in \ac{VHDL}, unlike the other caches that are written in
Verilog. Depending on the synthesis and simulation tools, one language can be
advantageous over the other but they are semantically equivalent. Most
open-source tools only allow one HDL, generally Verilog, so the entire project
needs to use the same language.

Compared to the presently developed IOb-Cache system, PoC.cache also lacks (1) a
front-end module to avoid the need to implement an FSM for processor-cache
communication; (2) a configurable back-end module that controls the
communication with the main memory, freeing the main-controller of unnecessary
delays; (3) a universally adapted memory interface like AXI. All these lacking
features, plus the fact it written in VHDL, have motivated IObundle to develop a
new open-source cache in Verilog.



\section{Cache: Basic Overview}
\label{chapter:cache_overview}

With the development of integrated systems, there is a necessity for larger
memories and speed. While processors keep getting faster, the increase of
capacity in memories results is progressive slower
accesses\cite[p.~634]{bib:ac}. This causes a bottleneck
effect\cite[p.~4]{bib:cache_study}, as the processor performance is dependent on
the speed of the memory accesses.

A cache is a smaller memory (compared to the main memory), installed close to
the processor with faster access time. Instead of accessing the main memory, the
processor accesses the cache most of the time, drastically decreasing the
average access time. Usually, the large main memory is implemented with
\ac{DRAM} technology, which is cheaper per byte stored but slower- On the other
hand, caches are implemented with \ac{SRAM} technology, which is more expensive
but considerably faster to access.

The cache works under two principles of locality: Temporal Locality and Spatial
Locality~\cite[p.633]{bib:ac}. Temporal Locality describes that if a specific
memory address is accessed, it is likely it will be accessed again.  Spatial
Locality states that after a specific memory address is accessed, it is likely
that an address close to it will be accessed.

A cache's main structure is composed of lines and words. A cache line represents
a memory block from the main memory, which is a sequential data block from a
specific location.  Each line is composed of words, instructions, or data
requested by the processor, which is limited by the size of its registers (a
32-bit processor has 32-bit words). The cache line storing a recently accessed
memory block capitalizes on both spatial and temporal localities.

When the cache is accessed, the requested data can be stored in it or not.  This
access is called a hit or miss access, respectively. There are 3 types of
misses: Compulsory, Capacity, and Conflict \cite[p.~9]{bib:cache_study}.

A compulsory miss happens when a cache-line is accessed for the first time,
since the cache was empty or invalidated.

A capacity miss occurs because the cache is smaller than the main memory and is
not big enough to store all the necessary data. After filling, the next access
will result in a capacity miss, as the data in some line must be discarded to load
the requested data from the main memory.

A conflict miss happens when two memory blocks are mapped to the same line,
causing the most recently accessed to replace the older one. The replaced line
can be required later eventually causing another conflict miss.

A fourth miss type exists for multiprocessing systems: the coherency miss.  This
miss happens when the main memory block is updated by an external source. If that
block is stored in the cache(s), it needs to be invalidated. When requested, the
newly updated block is fetched.

Instead of having a fixed main memory access time, a cached system has the
following average memory access time\cite[p.~6]{bib:cache_study}:

\begin{multline}
  average\_access\_ time = hit\_ratio \times average\_ cache\_ access\_time +
  \\ + miss\_ratio \times (average\_cache\_ access\_time + average\_ main\_
  memory\_ access\_ time)\,.
  \label{eq:aat}
\end{multline}

The hit-ratio is the percentage of cache-hits of all accesses, while
the miss-ratio the percentage of cache-misses of all accesses.  Of course they are
complementary: miss-ratio = 1 - hit-ratio.



\subsection{Mapping}
\label{chapter:mapping}

Mapping represents the cache's internal organization.  There are 3 used
mappings: direct-mapped, set-associative, and fully associative.

Direct-mapped represents the basic organization of a cache. The
address is divided into tag, index, and offset. The index addresses the cache
lines, while the offset addresses their respective words. Because of the smaller
capacity, not all address bits are used to address the cache and are used for
validation. These bits are called the Tag and are used to verify if the data
present in the cache has the same address as the requested data.

Besides the data memory, two additional memories are used, the Tag memory and
Valid memory.  The Tag memory stores the tag of that respective block, while the
Valid memory indicates that that position contains valid data for the main
memory. Initially, all positions are invalid until they are filled with valid
content. One may want to invalidate a certain memory position if a third entity
alters the contents of that memory position, which happens in multi-processor
systems. The requested address' tag is then compared with the Tag memory output.
If they match then a cache hit happens; otherwise, a cache miss occurs. In case
of a miss, the accessed line needs to be replaced with the block that contains
the requested data.

The set-associative mapping is based on the direct-mapped but uses
multiple ways to access data blocks in the same set. Each way is in theory an
individual direct-mapped cache. This means the same cache-line stores as
many tags as the number of ways. This reduces conflict-misses. During a
line-replacement, it uses a replacement policy to select the way to be replaced.

The fully associative mapping is different in that does not need an index to
address a line since it uses a single set. During access, every line is
verified for a tag match. This means all lines will have their tag compared and
validated. This totally removes conflict-misses. Since all tags are required to
be compared, cascaded comparators need to produce a hit, resulting in an
expensive hardware structure seldom used in practice. Like the set-associative
mapping, it uses a replacement policy to select the line to be replaced.

The most common replacement policy is the \ac{LRU}, which keeps track of how
recently each way has been accessed, replacing the least recent one.  Some
effective but less costly (requiring less information) replacement policies can
be used, such as the Pseudo-{LRU} policies. Of these, the most common
are the \ac{MRU}-based and tree-based Pseudo-{LRU} policies.

The MRU-based PLRU keeps track of the accessed ways. When all of them have been
accessed, all but the most recent way are reset to non-accessed. The lowest
indexed non-accessed way is the one elected to be replaced.


The tree-based PLRU uses a binary tree to keep track of the less recently used
way. Starting from the root node, each node points to the next level node,
creating a path towards a leaf that represents the least recently accessed
way. The nodes' pointers are updated (or not) when access happens, which may
change or not the path to point to a more recently used way. Each node uses 1
bit to point to two halves of a subset of ways, which may need to be toggled
upon an update.


\subsection{Write-Policy}
\label{chapter:write_policy}

During reads, if the data is not available in the cache, it is fetched from the
main memory.  Writes on the other hand have 2 different policies called
Write-Through and Write-Back.

With the Write-Through policy, the data is written to both the cache and the
main memory. This policy is normally associated with the
Write-Not-Allocate co-policy, meaning that, if the data is not available in the
cache, it is simply not stored.

With the Write-Back policy, the main-memory is only written any data when the
cache-line is replaced (or removed). This policy normally associated with the
Write-Allocate co-policy, meaning that if the address to write is not present in
the cache, the respective memory block is first fetched from the main memory to
the cache before writing the new data onto it.

Since the write-through updates the memory for every write-access, it generates
significant traffic to the main memory~\cite[p.~5]{bib:cache_study}, which is
the main disadvantage of this method.


\subsection{Cache-types}
\label{chapter:cache_types}

To improve performance, it is not enough simply to increase the cache size. A
higher-level organization of the cache can achieve this goal without adding more
on-chip memory. This organization can either exploit the type of accessed data,
instructions or data, or expand the memory hierarchy with multiple levels of
cache blocks.

Dividing caches into instruction or data caches enables parallel access to
instruction and data words and parallel exploitation of the localities that
exist in instruction and date address sequences. This greatly reduces
unnecessary conflict-misses.

In a multi-level cache system, the idea is to reduce the miss penalty, that is,
the cost in time of a cache miss. Accessing the external memory upon a cache
miss represents the highest penalty. Placing a second cache in between the
first-level cache and the main memory attenuates the miss penalty, as the time
cost of accessing the second cache should be much lower than that of accessing
the main memory. Normally the size of the cache increases with the level. A
typical configuration used in this work is to use two level-1 (L1) caches for
instructions and data and a larger level-2 cache of unified instructions and
data. Different replacement and write-policies can be used on different
levels, which can be optimized to greatly improve performance.
