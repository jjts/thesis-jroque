\section{Back-End}
\label{chapter:back_end}

The Back-End module is the interface between the cache and the main memory.
There are currently 2 available main memory interfaces: Native and AXI. The
native interface follows a pipelined valid-ready protocol and is shown in
Figure~\ref{fig:back_end_native}. The AXI interface implements the \ac{AXI4}
protocol~\cite{bib:axi_ref,bib:axi_guide} and can be seen in
Figure~\ref{fig:back_end_axi}.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Figures/back_end_native}
  \caption{Back-End Native module diagram.}
  \label{fig:back_end_native}
\end{figure}

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.5\linewidth]{Figures/back_end_axi}
  \caption{ Back-End AXI module diagram.}
  \label{fig:back_end_axi}
\end{figure}

Although the AXI interface has independent write and read buses, the native
interface only has a single bus available. In the native interface, the
difference between a write and read access depends on the write-strobe signal
(mem\_wstrb) being active or not. This requires additional logic to select which
controller accesses the main memory. The AXI interface transfers can be
configured using the relevant synthesis parameters given in
Table~\ref{table:parameters}. There is no risk of conflict between the
read and write channels: reading for line replacement can only occur after all
pending writes are done.

The Back-End module has two controllers, the Write-Channel controller and the
Read-Channel controller. The Write-Channel controller reads data from the
Write-Through Buffer and writes data to the main memory while the buffer is not
empty. The Read-Channel controller fetches lines from the main memory and writes
them to the cache during a cache line replacement.


\subsection{Write-Channel Controller}
\label{chapter:write_channel}
The Write-Channel controller ports for both the native and AXI4 interfaces are
described in Table~\ref{table:write_channel}.

\begin{table}[h!t!b]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Write-Channel ports.}
  \label{table:write_channel}
  \centering
  \begin{tabular}{||l||>{\raggedleft}p{2.5cm}|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\
    \hline
    \multicolumn{4}{|c|}{\textbf{Cache Side Native Interface}}\\\hline
    \textbf{write\_valid} & 1 & output & Valid data in write-through buffer (not empty). \\\hline
    \textbf{write\_addr} & FE\_ADDR\_W -FE\_BYTE\_W  & input & Word-address of the write data. Buffer's ouput.\\\hline
    \textbf{write\_wdata} & FE\_DATA\_W & input & Write-Data. Buffer's output \\\hline
    \textbf{write\_wstrb} &  & input & Write-strobe. Buffer's output \\\hline
    \textbf{write\_ready} &  & input & Ready, read write through buffer. \\\hline
    \multicolumn{4}{|c|}{\textbf{Memory Side Native Interface}}\\\hline
    \textbf{mem\_valid} & 1 & output & Validates the write transfer to the main-memory.\\\hline
    \textbf {mem\_addr} & BE\_ADDR\_W & output & Address of the write transfer.\\\hline
    \textbf {mem\_wdata} & BE\_DATA\_W & output & Write-Data. Data transfered to memory.\\\hline
    \textbf {mem\_wstrb} & BE\_NBYTES & output & Validates mem\_wdata's Bytes.\\\hline
    \textbf {mem\_ready} & 1 & input & Memory's ready, acknowledges the transfer. \\\hline
    \multicolumn{4}{||c|}{\textbf{Memory Side AXI Interface}}\\\hline
    \textbf {axi\_awvalid} & 1 & output & Validates Write-Address.\\\hline
    \textbf {axi\_awaddr} & BE\_ADDR\_W & output & Write-Address, Byte addressable and word aligned.\\\hline
    \textbf {axi\_awready} & 1 & input & Acknowledges the address request.\\\hline
    \textbf {axi\_wvalid} & 1 & output & Validates the data transfer.\\\hline
    \textbf {axi\_wdata} & BE\_DATA\_W & output & Write-Data. \\\hline
    \textbf {axi\_wstrb} & BE\_NBYTES & output & Validates Write-Data's Bytes. \\\hline
    \textbf {axi\_wready} & 1 & input & Memory's acknowledges the transfer.\\\hline
    \textbf {axi\_bvalid} & 1 & input & Write response valid, validates axi\_bresp. \\\hline
    \textbf {axi\_bresp} & 2 & input & Write response result, if the transfer was successful ("00" - OKAY)~\cite[p.~A3-54]{bib:axi_ref}.\\\hline
    \textbf {axi\_bready} & 1 & output & Write response ready signal, awaits for the response result (handshake).\\\hline
    \textbf {axi\_awlen} & 8 & output & Burst's length, 0 (single)~\citep[p.~A3-48,51]{bib:axi_ref, bib:axi_guide}).\\\hline
    \textbf {axi\_awsize} & 3 & output & Bytes per beat, $log_2(BE\_NBYTES)$~\cite[p.~A3-49]{bib:axi_ref} \\\hline
    \textbf {axi\_awburst} & 2 & output & Burst type, any option (single transfer)~\citep[p.~A3-49]{bib:axi_ref}).\\\hline
    \textbf {axi\_awid} & AXI\_ID\_W & output & AXI identification tag~\cite[p.~A5-81]{bib:axi_ref}.\\\hline
    \textbf {axi\_awlock} & 1 & output & Atomic access. Normal access $0_b$~\cite[p.~A7-100]{bib:axi_ref}.\\\hline
    \textbf {axi\_awcache} & 4 & output & Type of memory. Default $0011_b$~\cite[p.~A4-68]{bib:axi_ref}.\\\hline
    \textbf {axi\_awprot} & 3 & output & All accesses are normal, $000_b$~\citep[p.~A4-75]{bib:axi_ref, bib:axi_guide}.\\\hline
    \textbf {axi\_awqo}& 4 & output & QoS identifier, unused, $0000_b$~\cite[p.~A8-102]{bib:axi_ref}.\\\hline
  \end{tabular}
\end{table}
\clearpage

The native interface's controller follows the control flow displayed in
Figure~\ref{fig:write_channel_native_fsm}. The controller stays in the initial
state while waiting for the write-through buffer to have data.  The write-through buffer uses a FIFO, and the FIFO starts the controller when it is not
empty.  When that happens, signal write\_valid asserts, and the FIFO gets read.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.8\linewidth]{Figures/write_channel_native_fsm}
  \caption{Back-End Write-channel Native Control-flow.}
  \label{fig:write_channel_native_fsm}
\end{figure}


In the following clock cycle, the required data is available in FIFO's output
and the transfer can occur.  After each transfer, the FIFO is checked, and if it
is not empty, it is read again so the data can be transferred in the following
clock cycle.  This keeps happening until there are no more available data in the
Write Through Buffer, and the controller goes back to its initial state.

The write-through buffer can only be read after each transfer is completed
(mem\_ready received). Currently, there is no way to pipeline these transfers,
which are limited to 1 word per every 2 clock cycles. While the controller is in
the initial state, the memory's write-strobe signal is 0 to not disturb the
Read-Channel controller.

The AXI-Interface (Figure~\ref{fig:write_channel_axi_fsm}) has similar behavior
but follows the AXI4 protocol.  The address valid-read handshake needs to happen
before any data can be transferred.  After the data is transferred, it is checked
to see if it was successful through the response channel (B channel): if
axi\_bresp does not have the OKAY value (an AXI code), then the transfer was
unsuccessful and the data is transferred again.

\begin{figure}[!h!tb]
  \centering \includegraphics[width=0.85\linewidth]{Figures/write_channel_axi_fsm}
  \caption{Back-End Write-channel AXI Control-flow.}
  \label{fig:write_channel_axi_fsm}
\end{figure}

If the Back-End's data width (BE\_DATA\_W) is larger than the front-end's
(FE\_DATA\_W), the data buses require alignment. The address signal becomes word-aligned, discarding the back-end's byte offset bits. These discarded bits are
used to align both the write data and strobe
(Figure~\ref{fig:write_channel_write_alignment}).

\begin{figure}[!h!tb]
  \centering
  \includegraphics[width=0.6\linewidth]{Figures/write_channel_write_alignment}
  \caption{Back-End Write-channel alignment.}
  \label{fig:write_channel_write_alignment}
\end{figure}

This results in Narrow transfers~\cite[p.~A3-49]{bib:axi_ref}, allowing the
smaller words to be transferred to a larger bus. The Write-Channel data width
is, therefore, limited to the cache's front-end word size. For example, in a 32-bit
system, connected to a 256-bit wide memory, each transfer will be limited to
32-bit anyway.
\clearpage


\subsection{Read-Channel Controller}
\label{chapter:read-channel}

The Read-Channel controller ports for both the native and AXI4 interfaces are
described in Table~\ref{table:read_channel}.

\begin{table}[!ht!b]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{Read-Channel ports.}
  \label{table:read_channel}
  \centering
  \begin{tabular}{||l||>{\raggedleft}p{2.5cm}|c|p{8cm}|}
    \hline
    \textbf{Parameter} & \textbf{Width (bit)} & \textbf{Direction} & \textbf{Description} \\
    \hline
    \multicolumn{4}{|c|}{\textbf{Cache Side Native Interface}}\\\hline
    \textbf{replace\_valid} & 1 & input & Requests a line replacement \\\hline
    \textbf{replace\_addr} & FE\_ADDR\_W -LINE2MEM\_W -BE\_BYTE\_W & output & Memory block's base address. \\\hline
    \textbf{replace} & 1 & output & Replacement in action. \\\hline
    \textbf{read\_valid} & 1 & output & Validates the Back-End Memory's transfered data. \\\hline
    \textbf{read\_addr} & LINE2MEM\_W & output & Addresses the placement in the cache line. \\\hline
    \textbf{read\_data} &  & input & Back-End Memory's transfered data. \\\hline
    \multicolumn{4}{|c|}{\textbf{Memory Side Native Interface}}\\\hline
    \textbf{mem\_valid} & 1 & output & Validates the read address.\\\hline
    \textbf {mem\_addr} & BE\_ADDR\_W & output & Read address of the memory block's words.\\\hline
    \textbf {mem\_rdata} & BE\_DATA\_W & input & Read data, used to update the cache line.\\\hline
    \textbf {mem\_ready} & 1 & input & Ready signal, validates the received read data.\\\hline
    \multicolumn{4}{|c|}{\textbf{Memory Side AXI Interface}}\\\hline 
    \textbf {axi\_arvalid} & 1 & output & Validates Read Address. \\\hline
    \textbf{axi\_araddr} & BE\_ADDR\_W & output &  AXI's Read Address, memory block's initial address. \\\hline
    \textbf {axi\_arready} & 1 & input & Acknowledges address request.\\\hline
    \textbf {axi\_rvalid} & 1 & input & Validates the read data sent by the memory.\\\hline
    \textbf {axi\_rdata} & BE\_DATA\_W & output & Read data. \\\hline
    \textbf {axi\_rready} & 1 & output & Cache acknowledges the transfer, asserted during line replacement.\\\hline
    \textbf {axi\_rresp} & 2 & input & Read response signal. Verifies if transfer was legal ("00" - OKAY).~\cite[p.~A3-54]{bib:axi_ref} \\\hline
    \textbf {axi\_arlen} & 8 & output &  Burst's length, depends on LINE2MEM\_W~\citep[p.~A3-48]{bib:axi_ref, bib:axi_guide}.\\\hline
    \textbf {axi\_arsize} & 3 & output & Bytes per beat, $log_2(BE\_NBYTES)$~\cite[p.~A3-49]{bib:axi_ref}.\\\hline
    \textbf {axi\_arburst} & 2 & output & Burst type, incremental ($01_{b}$)~\cite[p.~A3-49]{bib:axi_ref}.\\\hline
   \textbf {axi\_arid} & AXI\_ID\_W & output & AXI identification tag~\cite[p.~A5-81]{bib:axi_ref}.\\\hline
    \textbf {axi\_arlock} & 1 & output & Atomic access. Normal access $0_b$~\cite[p.~A7-100]{bib:axi_ref}.\\\hline
    \textbf {axi\_arcache} & 4 & output & Identifies the type of memory. Default $0011_b$~\cite[p.~A4-68]{bib:axi_ref}.\\\hline
    \textbf {axi\_arprot} & 3 & output & Permission for illegal transfers, unused. All accesses are normal, $000_b$~\citep[p.~A4-75]{bib:axi_ref, bib:axi_guide}.\\\hline
    \textbf {axi\_arqos}& 4 & output & QoS identifier, unused, $0000_b$~\cite[p.~A8-102]{bib:axi_ref}.\\\hline
    
  \end{tabular}
\end{table}
\clearpage

\begin{figure}[h]
  \centering
  \includegraphics[width=0.8\linewidth]{Figures/read_channel_native_fsm}
  \caption{Back-End Read-channel Native Control-flow.}
  \label{fig:read_channel_native_fsm}
\end{figure}

The native interface's controller follows the control flow displayed in
Figure~\ref{fig:read_channel_native_fsm}. The controller stays in the initial
state $S_0$ while waiting for the request of a line replacement.  When signal
``replace'' is asserted, the controller goes to state $S_1$ requests a word
block from the memory and writes it to the cache line at 1 word per cycle after
it arrives at the back-end. It requests the base address of the main
memory's memory block and uses a word counter to count the received words.
After the last word is received the controller goes to state $S_2$ for a single
cycle to compensate for the Data memory RAM's read latency. Afterward, it goes back
to its state $S_0$, de-asserting signal "replace".

If the back-end's data width (BE\_DATA\_W) is multiple the front-end's
(FE\_DATA\_W), the number of words counted is proportionally shorter. If the
back-end's data width is the same size as the entire cache line, the burst
length is 1 and therefore the word counter is not used.

The AXI interface controller (Figure~\ref{fig:read_channel_axi_fsm}) has a
similar behavior but uses AXI4 burst transfers. The AXI burst parameters are
derived for synthesis, using the front-end and back-end data widths, and the
cache line's offset width.  Instead of using a word counter, the signal
axi\_rlast is used to know when the line has been fully replaced. During the
burst, each beat (transfer) increments signal read\_addr automatically.

\begin{figure}[htb]
  \centering
  \includegraphics[width=0.9\linewidth]{Figures/read_channel_axi_fsm}
  \caption{Back-End Read-channel AXI Control-flow.}
  \label{fig:read_channel_axi_fsm}
\end{figure}

Unlike the Write-Channel controller, the response signal, "axi\_rresp", is sent
during each beat (transfer) of the burst.  This requires the use of a register
which sets in the case at least one of the beats was unsuccessful. After the
transfers, the verification of this register can be done at the same time as the
read latency compensation.

