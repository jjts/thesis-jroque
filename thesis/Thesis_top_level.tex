\section{IOb-Cache: top-level}
\label{chapter:iob_cache_top}


The top-level integrates all the IOb-Cache modules and is represented in the
Fig. \ref{fig:iob_cache}.

\begin{figure}[!h!tp]
  \centering
  \includegraphics[width=\linewidth]{Figures/iob_cache}
  \caption{IOb-Cache top-level module diagram.}
  \label{fig:iob_cache}
\end{figure}

The Front-End connects the cache to a Master (processor). The ports always use
the Native Interface, using a valid-ready protocol.

The Back-End connects the cache (master) to the main-memory (slave).  Its
interface depends on the choice of the top-level module: Native (iob\_cache) or \ac{AXI}
(iob\_cache\_axi).

Cache-Memory is shown in between Front-End and Back-End and contains
all the cache's memories and its main-controller.

Cache-Control is an optional module for an L1 cache that allows performing tasks
such as invalidating a data cache, requesting its Write-Through Buffer's status,
or analyzing its hit/miss performance. If the "CTRL\_IO" macro is set,
interfaces for invalidating the cache or observe the Write-Through-Buffer's
status are implemented without the Cache-Control module, which is useful for
cascading higher-level caches.

The many synthesis parameters that are available for IOb-Cache are shown in
Table~\ref{table:parameters}.

\begin{table}[!h!tp]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{IOb-cache's configurable parameters.}
  \label{table:parameters}%
  \centering
  \begin{tabular}{||l||p{12cm}|}
    \hline
    \textbf{Parameter} & \textbf{Description} \\
    \hline
    \textbf{FE\_ADDR\_W}                         & Front-End Address Width: defines how many bytes are accessible in the main memory ($2^{FE\_ADDR\_W}$ bytes). \\\hline
    
    \textbf{FE\_DATA\_W}                         & Front-End Data Width: defines the cache word-size. Needs to be multiple of 8 (byte). Independent of the Back-end's data bus width (BE\_DATA\_W). \\\hline
    
    \textbf{N\_WAYS}                               & Number of (Cache) Ways: Direct Mapped(1); Set-Associative Mapped (\textgreater{}1). Needs to be power of 2.\\\hline
    
    \textbf{LINE\_OFF\_W}                          & Line Offset Width or Index Width: Defines the number of cache lines. $2^{LINE\_OFF\_W}$ lines.\\\hline
    
    \textbf{WORD\_OFF\_W}                          & Word Offset Width: Number of Words per cache line. $2^{WORD\_OFF\_W}$ words/line.\\\hline
    
    \textbf{REP\_POLICY}                           & Replacement Policy: LRU (0); PLRUm (1); PLRUt (2). Requires N\_WAYS \textgreater 1.\\\hline
    
    \textbf{WTBUF\_DEPTH\_W}                       & Write-Through-Buffer Depth Width: Number of positions in write-through buffer's FIFO, $2^{WTBUF\_DEPTH\_W}$.\\\hline
    
    \textbf{BE\_ADDR\_W}                           & Back-End Address Width: defines the width of the back-end address port, additional bits aren't accessible (access depends on FE\_ADDR\_W).\\\hline
    
    \textbf{BE\_DATA\_W}                           & Back-End Data Width: Back-End's memory word-size. Needs to be multiple of  FE\_DATA\_W. This can be used to increase bandwidth.\\\hline
    
    \textbf{CTRL\_CACHE}                           & Implementation of Cache-Control: performance measurement, write-through buffer status and cache invalidate.\\\hline
    
    \textbf{CTRL\_CNT}                             & Implements counters for performance measurement (requires CTRL\_CACHE(1)).\\\hline
    
    \textbf{AXI\_ID}                               & AXI-Identifier: Sets the value for the "axi\_**id" signals for AXI back-end connections.\\\hline
    
    \textbf{AXI\_ID\_W}                            & Defines the Width of AXI-Identifier. "AXI\_ID's" value needs to be in this range.\\\hline
  \end{tabular}
\end{table}

From the configurable parameters, a few derived parameters of interest are
computed. They are explained in Table~\ref{table:parameters_unconfig}

\begin{table}[!h!tp]
  \renewcommand{\arraystretch}{1.5} %more space between rows
  \caption{IOb-cache's derived parameters.}
  \label{table:parameters_unconfig}%
  \centering
  \begin{tabular}{||l||p{12cm}||}
    \hline
    \textbf{Parameter}          & \textbf{Description}\\
    \hline
    \textbf{FE\_NBYTES}         & Front-End Number of Bytes: The number of bytes in FE\_DATA\_W. $FE\_NBYTES = \frac{FE\_DATA\_W}{8}$ \\\hline
    \textbf{FE\_BYTE\_W}        & Front-End Byte Offset Width. $FE\_BYTE\_W = \log_2(FE\_NBYTES)$ \\\hline

    \textbf{NWAY\_W}            & Width of Number of Ways. $NWAY\_W = \log_2 (N\_WAYS)$ \\\hline

    \textbf{BE\_NBYTES}         & Back-End Number of Bytes: The number of bytes in BE\_DATA\_W. $BE\_NBYTES = \frac{BE\_DATA\_W}{8}$\\\hline

    \textbf{BE\_BYTE\_W}        & Back-End Byte Offset Width. $BE\_BYTE\_W = \log_2(BE\_NBYTES)$  \\\hline

    \textbf{LINE2MEM\_W}         & Line-to-Memory Word-Size Ratio Width: the width of the ratio between the size of the cache-line and the back-end's word. $LINE2ME\_W = log_2(\frac{WORD\_OFF\_W*FE\_DATA\_W}{BE\_DATA\_W})$ \\\hline
  \end{tabular}
\end{table}
